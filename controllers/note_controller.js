const Note = require('../models/Note');

class NoteController {
  async getNotes(req, res) {
    try {
      const notes = await Note.find();
      await res.status(200).json({notes,
        offset: 0,
        limit: 0,
        count: 0});
      console.log(notes);
      if (!notes) {
        return res.status(400).json({
          message: 'Not found notes',
        })
      }
    } catch (err) {
      err = 'Server error'
      await res.status(500).json({message: err});
    }
  }

  async addNote(req, res) {
    try {
      const {id} = req.user;
      const note = await Note.create({
        userId: id,
        text: req.body.text,
      });
      await note.save();
      if (!note) {
        return res.status(400).json({
          message: `Not found note with ID:${req.params.id}`,
        });
      }
      await res.status(200).json({
        message: "Success"
      });
    } catch (err) {
      console.log(err);
      await res.status(500).json(err);
    }
  }

  async getNote(req, res) {
    try {
      const note = await Note.findOne({_id: req.params.id});
      await res.status(200).json({note});
      console.log({note});
    } catch (err) {
      if (err.code === 'ENOENT') {
        console.error(err);
        res.status(400).json({message: `No file with ${req.params.id}ID found`});
      } else {
        console.error(err);
        res.status(500).json({
          message: "Server error"
        });
      }
    }
  }

  async updateNote(req, res) {
    try {
      const putNote = await Note.update(
          {_id: req.params.id}, {$set: {text: req.body.text}},
      );
      if (!putNote) {
        return res.status(400).json({
          message: `Not found note with ID:${req.params.id}`,
        });
      }
      await res.status(200).json({
        message: "Success"
      });
      console.log({putNote});
    } catch (err) {
      err = 'Server Error';
      console.log(err);
      await res.status(500).json({message: err});
    }
  }

  async checkNote(req, res) {
    try {
      const note = await Note.findOne({_id: req.params.id});
      if (note) {
        note.completed = !note.completed;
      }
      if (!note) {
        return res.status(400).json({
          message: `Not found note with ID:${req.params.id}`,
        });
      }
      await note.save();
      await res.status(200).json({
        message: "Success"
      });
    } catch (err) {
      console.log(err);
      err = 'Server Error';
      await res.status(500).json({message: err});
    }
  }

  async deleteNote(req, res) {
    const deleteNote = await Note.deleteOne({_id: req.params.id});
    res.status(200).send({message: 'Success'});
    console.log(deleteNote);
  }
}

module.exports = new NoteController();
