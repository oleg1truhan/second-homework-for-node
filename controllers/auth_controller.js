const User = require('../models/User');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const {validationResult} = require('express-validator');
const {secret} = require('../config');

const generateAccessToken = (id, username, password) => {
  const payload = {
    id,
    username,
    password,
  };
  return jwt.sign(payload, secret, {expiresIn: '24h'});
};

class AuthController {
  async registration(req, res) {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({message: 'Error'});
      }
      const {username, password} = req.body;
      const candidate = await User.findOne({username});
      if (candidate) {
        return res.status(400).json({
          message: 'User with this name already exists',
        });
      }
      const hashPassword = bcrypt.hashSync(password, 7);
      const user = new User({username, password: hashPassword});
      await user.save();
      return res.json({message: 'Success'});

    } catch (e) {
      console.error(e);
      await res.status(500).json({message: 'Registration error'});
    }
  }

  async login(req, res) {
    try {
      const {username, password} = req.body;
      const user = await User.findOne({username});
      if (!user) {
        return res.status(400).json({message: `${username} not found`});
      }
      const validPassword = bcrypt.compareSync(password, user.password);
      if (!validPassword) {
        return res.status(400).json({message: `Enter incorrect password`});
      }
      const token = generateAccessToken(user._id, user.username, user.password);
      return res.json({message: "Success", jwt_token: token});
    } catch (e) {
      console.error(e);
      await res.status(500).json({message: 'Login error'});
    }
  }

  async getUsers(req, res) {
    try {
      const users = await User.find();
      await res.json(users);
    } catch (e) {
      console.error(e);
    }
  }
}

module.exports = new AuthController();
