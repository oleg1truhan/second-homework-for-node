const User = require('../models/User');
const bcrypt = require('bcryptjs');

class UserController {
  async getUsers(req, res) {
    try {
      const {id} = req.user;
      const users = await User.findOne({_id: id});
      const now = new Date();
      await res.status(200).json({
        'user': {
          '_id': users._id,
          'username': users.username,
          'createdDate': now,
        },
      });
      console.log(users);
    } catch (err) {
      if (err.code === 'ENOENT') {
        console.error(err);
        res.status(400).json({message: `No file with ${id}ID found`});
      } else {
        err = 'Server error';
        console.error(err);
        res.status(500).json({
          message: "Server error"
        });
      }
    }
  }

  async deleteUser(req, res) {
    const {id} = req.user;
    try {
      const deleteUser = await User.deleteOne({_id: id});
      res.status(200).send({message: 'Success'});
      console.log(deleteUser);
    }catch (err) {
      if (err.code === 'ENOENT') {
        console.error(err);
        res.status(400).json({message: `No file with ${id}ID found`});
      } else {
        err = 'Server error';
        console.error(err);
        res.status(500).json({
          message: "Server error"
        });
      }
    }
  }

  async changePassword(req, res) {
    try {
      const {id, password} = req.user;
      const patchUsers = await User.findOneAndUpdate(
          {_id: id},
          {$set: {password: bcrypt.hashSync(req.body.newPassword, 7)}},
      );
      if (password === req.body.newPassword &&
          req.body.newPassword.length < 4) {
        return res.status(400).json({
          message: 'Please specify "password" parameter',
        });
      }
      res.status(200).send({message: 'Success'});
      console.log(patchUsers);
    } catch (err) {
      console.log(err);
      await res.status(500).json({message: err});
    }
  }
}

module.exports = new UserController();
