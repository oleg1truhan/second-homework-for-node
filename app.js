const express = require('express');
const mongoose = require('mongoose');
const userRouter = require('./routers/user_router');
const authRouter = require('./routers/auth_router');
const noteRouter = require('./routers/note_router');
const PORT = 8080;
const app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use('/api', userRouter);
app.use('/api', authRouter);
app.use('/api', noteRouter);

const start = async () => {
  try {
    await mongoose.connect('mongodb+srv://Oleshka_der:qwerty123@cluster0.oytzn.mongodb.net/users', {useNewUrlParser: true});
    app.listen(PORT, () => {
      console.log('Server has been started');
    });
  } catch (e) {
    console.log(e);
  }
};

start();
