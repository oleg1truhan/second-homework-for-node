const {Router} = require('express');
const noteRouter = new Router();
const noteController = require('../controllers/note_controller');
const authMiddleWare = require('../authMiddleware/authMiddleware');

noteRouter.get('/notes', noteController.getNotes);

noteRouter.post('/notes', authMiddleWare, noteController.addNote);

noteRouter.get('/notes/:id', authMiddleWare, noteController.getNote);

noteRouter.put('/notes/:id', authMiddleWare, noteController.updateNote);

noteRouter.patch('/notes/:id', authMiddleWare, noteController.checkNote);

noteRouter.delete('/notes/:id', authMiddleWare, noteController.deleteNote);

module.exports = noteRouter;
