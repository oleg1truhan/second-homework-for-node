const Router = require('express');
const authRouter = new Router();
const authController = require('../controllers/auth_controller');
const {check} = require('express-validator');
const authMiddleWare = require('../authMiddleware/authMiddleware');

authRouter.post('/auth/register', [
  check('username', 'Username should not be empty').notEmpty(),
  check('password', 'Password must be long 4 and short 10 symbols')
      .isLength({min: 4, max: 10}),
], authController.registration);
authRouter.post('/auth/login', authController.login);
authRouter.get('/users', authMiddleWare, authController.getUsers);

module.exports = authRouter;
