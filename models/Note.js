const {Schema, model} = require('mongoose');

const Note = new Schema({
  completed: {type: Boolean, required: true, default: false},
  userId: {type: Schema.Types.ObjectId, ref: 'User'},
  text: {type: String, required: true},
  createData: {type: Date, default: Date.now()},
}, {versionKey: false},
);

module.exports = model('Note', Note);

