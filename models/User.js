const {Schema, model} = require('mongoose');

const User = new Schema({
  id: {type: Number},
  username: {type: String, unique: true, required: true},
  password: {type: String, required: true},
}, {versionKey: false}
);

module.exports = model('User', User);
